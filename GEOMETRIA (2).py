""" ___autora___= "Valeria Guerero"
    ___email___"valeria.guerrero@unl.edu.ec"

Ejercicio 01:
             >> CREE  UN MÓDULO PYTHON  LLAMADO "geometria" Y EN EL REALICE LO SIGUIENTE:
             >> Crear una clase llamada Punto con sus dos coordenadas X e Y.
             >> Añadir un método constructor para crear puntos fácilmente. Si no se reciben una coordenada,
                su valor será cero.
             >> Sobreescribir el método __str__, para que al imprimir por pantalla un punto aparezca en formato (X,Y)
             >> Añadir un método llamado cuadrante que indique a qué cuadrante pertenece el punto, teniendo en cuenta
                que si X == 0 e Y != 0 se sitúa sobre el eje Y, si X != 0 e Y == 0 se sitúa sobre el eje X y si X == 0
                Y == 0 está sobre el origen.
             >> Añadir un método llamado vector, que tome otro punto como parámetro y calcule el vector resultante
                entre los dos puntos.
             >> Añadr un método llamado distancia, que tome otro punto como parámetro y calcule la distancia entre
                los dos puntos y la devuelva. Recuere la fórmula:
             >> Crear una clase llamada Rectangulo con dos puntos (inicial y final) que formarán la diagonal del
                 rectángulo.
             >> Añadir un método constructor para crear ambos puntos fácilmente, si no se envían se crearán dos
                 puntos en el origen por defecto.
             >> Añadir al rectángulo un método llamado base que devuelva la base.
             >> Añade al rectángulo un método llamado altura que devuelva la altura.
             >> Añade al rectángulo un método llamado area que devuelva el area."""


import math
import pygame
import matplotlib.pyplot as plt
import numpy as np

import pygame

blanco = 255, 255, 255
verde = 10, 233, 98
pygame.init()
ventana = pygame.display.set_mode((500,400),0,32)
# Cambio el título de la ventana


class Punto:

    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return 'I'
        elif self.x < 0 and self.y > 0:
            return 'II'
        elif self.x < 0 and self.y < 0:
            return 'III'
        elif self.x > 0 and self.y < 0:
             return 'IV'
        elif self.x != 0 and self.y == 0:
             return "{} se encuentra sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se encuentra sobre el eje Y"
        else:
             return "sobre el origen"

        return self.x and self.y

    def vector(self, p):
        vector = Punto(p.x - self.x, p.y - self.y)
        return vector

    def distancia(self, p):
        distancia = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        return distancia

    def __str__(self):
        return "({}, {})".format(self.x, self.y)


class Rectangulo:
    punto_inicial= None
    punto_final= None

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def area(self):
        return self.base()*self.altura()
class ParOrdenado:
    def __init__(self, a, b):
        self.real = int(a)
        self.imaginario = int(b)

# La funcion graficar se supone debe tomar esos valores y usarlos para
# crear un vector que vaya de 0 a el componente respectivo de X y Y


def graficarComp(e):
    # Coordenadas del vector
    x, y = e.real, e.imaginario

    # Limites de la figura
    izda = min(-1, x-1)
    dcha = max(1, x+1)
    abajo = min(-1, y-1)
    arriba = max(1, y+1)

    # El metodo quiver pinta vectores, pero para que salgan de las
    # dimensiones correctas hay que usar los parámetros angles, scale y scale_units

    plt.quiver([x], [y], angles='xy', scale_units='xy', scale=1)

    # Pintamos lineas que pasan por el origen de coordenadas
    plt.axhline(0, color='red')
    plt.axvline(0, color='red')

    # Fijamos límites, etiquetas y títulos
    plt.xlim([izda, dcha])
    plt.ylim([abajo, arriba])
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('({},{})'.format(e.real, e.imaginario))
    plt.show()

if __name__ == '__main__':

    A = Punto(2, 3)
    B = Punto(5, 5)
    C = Punto(-3, -1)
    D = Punto(0, 0)

    print (f"El punto {A} se encuentra en el cuadrante {A.cuadrante()}")
    print (f"El punto {C} se encuentra en el cuadrante {C.cuadrante()}")
    print (f"El punto {D} se encuentra {D.cuadrante()}")
    print (f"La distancia entre el punto {A} y {B} es {A.distancia(B)}")
    print (f"La distancia entre el punto {B} y {A} es {B.distancia(A)}")

    da= A.distancia(D)
    db= B.distancia(D)
    dc= C.distancia(D)
    if da > db and da > dc:
        print (f"El punto {A} se encuentra más lejos del origen")
    elif db > da and db > dc:
        print(f"El punto {B} se encuentra más lejos del origen")
    else:
        print(f"El punto {C} se encuentra más lejos del origen")

    rect = Rectangulo(A, B)
    print("La base del rectangulo es {}".format(rect.base()))
    print("La altura del rectangulo es {}".format(rect.altura()))
    print("El area del rectangulo es {}".format(rect.area ()))

    ejemplo = ParOrdenado(5, 5)
    ejemplo1 = ParOrdenado(-3, -1)
    ejemplo2 = ParOrdenado(2, 3)
    graficarComp(ejemplo)
    graficarComp(ejemplo1)
    graficarComp(ejemplo2)

    # Cambio el título de la ventana
    pygame.display.set_caption(" RECTANGULO")
    # dibujo
    ventana.fill(blanco)
    # dibujo rectangulo con el modulo draw
    pygame.draw.rect(ventana, verde, (2, 3, 200, 300))

    # actualizar
    pygame.display.update()
    # cerrar ventana
    running = True
    while running:
        for event in pygame.event.get():
            # evento para cerrar la ventana
            if event.type == pygame.QUIT:
                running = False

    # salir
    pygame.quit()




